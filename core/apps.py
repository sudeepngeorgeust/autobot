from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'core'
    
    def ready(self):
        import core.models.basemodels.thresholds.evaluate_threshold
