from django.db import models
from core.models.basemodels import Metric,Monitoring,Threshold

class SimpleThreshold(Threshold):
    condition_choices = (('lt', 'Less than'), ('gt', 'Great than'))
    condition = models.CharField(max_length=2, choices=condition_choices, default='lt')
    reoccurence = models.IntegerField(default=1)
    value = models.DecimalField(max_digits=15, decimal_places=2,blank=True,null=True)


    def evaluate_threshold(self):
        metric_value = self.monitoring_engine.get_latest_metric(self.metric,self.application)
        if self.condition == 'lt' :
            if  int(float(metric_value)) < int(float(self.value)): 
                [ action.perform_action(self.application) for action in self.actions.all() ]
        else:
            if int(metric_value) > int(self.value): 
                [ action.perform_action(self.application) for action in self.actions.all() ]

