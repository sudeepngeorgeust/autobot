from core.models.basemodels.threshold import Threshold

def evaluate(threshold_id):
    threshold = Threshold.objects.get_or_create(id=threshold_id)[0]
    threshold.evaluate_threshold()