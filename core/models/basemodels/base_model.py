
from polymorphic.models import PolymorphicModel
from django.utils import timezone
from django.db import models

class BaseModel(PolymorphicModel):
    class Meta:
        abstract = True
    
    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.now()
        self.date_modified = timezone.now()
        return super(BaseModel, self).save(*args, **kwargs)