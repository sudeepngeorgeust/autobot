from core.models.basemodels.base_model import BaseModel
from core.models.basemodels.threshold import Threshold
from django.db import models
from abc import abstractmethod
class Action(BaseModel):
    name = models.TextField(max_length=30)    
    date_created = models.DateTimeField(editable=False)
    date_modified = models.DateTimeField()
    
    #To support multiple actions  for a single Threshold  (One-to-Many from Threshold to actions)
    threshold = models.ForeignKey(Threshold, on_delete=models.CASCADE, related_name='actions')

    @abstractmethod
    def perform_action(self,application):
        pass