from .base_model import BaseModel
from .credential import Credential
from .load_balancer import LoadBalancer
from .metric_store import MetricStore
from djongo import models

from enum import Enum
class MonitoringStates(Enum):   # A subclass of Enum
    Running = "Running"
    Not_Running = "Not Running"
    Error = "Error"

class Application(BaseModel):
    monitoring_status =  models.CharField(
        max_length=15,
        choices=[(tag.value, tag.value) for tag in MonitoringStates] , # Choices is a list of Tuple        
        default=MonitoringStates.Not_Running.value,
    )
    name = models.CharField(max_length=50,unique=True)

    loadbalancer = models.ForeignKey(LoadBalancer,on_delete=models.CASCADE,blank=True,null=True)
    # TO-DO : check on_delete rule cascade or protect.
    # Consider changing the foreign key attribue to M2M if credentials is a list.
    credential = models.ForeignKey(Credential,on_delete=models.CASCADE,blank=True,null=True)

    metric_store = models.ForeignKey(MetricStore,on_delete=models.CASCADE,blank=True,null=True)

    #Applucation object has few childs connected via reverse Foreign key relationships to achieve One-To-Many relation
    # 1. Instances 
    # 2. Metric Store
    # 3. Monitoring Engine
    # 4. Metrics

    def __str__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        # if self.monitoring_status == MonitoringStates.Running.value:
        #     self.stop_monitoring()
        #     self.start_monitoring()
        super(Application, self).save(force_insert, force_update, *args, **kwargs)

    def start_monitoring(self,*args):
        # All in one single queue job
        # Check for application thresholds
        # Check threshold satisifed
        # Perform action corresponding to threshold.
        self.monitoring_status = MonitoringStates.Running.value
        self.save()
        for threshold in self.thresholds.all():
            threshold.get_real_instance().start_evaluating_threshold()
            pass
    def stop_monitoring(self,*args):
        self.monitoring_status = MonitoringStates.Not_Running.value
        self.save()
        for threshold in self.thresholds.all():
            threshold.get_real_instance().stop_evaluating_threshold()
            pass


