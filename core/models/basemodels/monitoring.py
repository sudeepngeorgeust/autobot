from core.models.basemodels.base_model import BaseModel
from django.db import models
from .application import Application
import logging
logger = logging.getLogger(__name__)

class Monitoring(BaseModel):
    period = models.IntegerField()
    keep_data = models.BooleanField(default=False)
    
    #To support multiple instances for a single application (One-to-Many from application to instance)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, related_name='monitoring_engines')

    
    def save_metric(self,data,application):
        try:
            print("try to save")
            # application.metric_store.insert(data)
        except Exception as ex:
            logger.exception(ex)
            raise ex