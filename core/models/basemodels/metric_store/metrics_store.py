from core.models.basemodels.base_model import BaseModel
from django.db import models
from django.utils import timezone

class MetricStore(BaseModel):
    def save(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.now()
            self.date_modified = timezone.now()
            return super(MetricStore, self).save(*args, **kwargs)

    def insert(self,data):
        #data format
        #{Timestamp:<Timestamp>,Metric:{Name:<MetricName>,Unit:<unit>,Value:<value>}}
        pass

    def bulk_insert(self, data):
        pass

    def get(self,id):
        pass

    def search(self, body):
        pass