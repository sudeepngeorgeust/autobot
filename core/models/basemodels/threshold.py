from core.models.basemodels.base_model import BaseModel
from django.db import models
from .metric import Metric
from .monitoring import Monitoring
from .application import Application
from django_q.tasks import schedule,async_task
from django_q.models import Schedule
from abc import abstractmethod
from django.core.exceptions import ObjectDoesNotExist

class Threshold(BaseModel):
    # If you’d prefer Django not to create a backwards relation, set related_name to '+' or end it with '+'
    metric = models.ForeignKey(Metric, on_delete=models.PROTECT,related_name="+")
    monitoring_engine = models.ForeignKey(Monitoring, on_delete=models.PROTECT,related_name="+")
    on_failure_continue = models.BooleanField(default=False)
    #threshold re-check interval In seconds
    recheck_interval = models.IntegerField(default=600) 
    #django-q task id for
    schedule_id = models.IntegerField()
    #To support multiple instances for a single application (One-to-Many from application to instance)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, related_name='thresholds')

    # __orginal_recheck_interval is added to check whether the model value recheck_interval has changed on save of threshold object
    # if changed restart the schedule with new period
    __orginal_recheck_interval = None

    def __init__(self, *args, **kwargs):
        super(Threshold, self).__init__(*args, **kwargs)
        self.__orginal_recheck_interval = self.recheck_interval

    def save(self, force_insert=False, force_update=False, *args, **kwargs):
        if self.recheck_interval != self.__orginal_recheck_interval:
            # Recheck interval changed, Update schedule period. 
            print("Restart schedule with new period")        
            try:
                schedule = Schedule.objects.get(id = self.schedule_id)
                schedule.minutes =  (self.recheck_interval/60)
                schedule.save()
            except Schedule.DoesNotExist:
                schedule = None                
        super(Threshold, self).save(force_insert, force_update, *args, **kwargs)
        self.__orginal_recheck_interval = self.recheck_interval

    #Threshold object has following childs connected via reverse Foreign key relationships to achieve One-To-Many relation
    # 1. Action
    @abstractmethod
    # Mandatory : To be implemented in subclasses
    def evaluate_threshold(self,metrics,monitoring_engine,):
        # Objective of this function is to implement in subclasses to evaluate the threshold values 
        # and perform all of the actions based on the threshold conditions  
        raise NotImplementedError                    

    def start_evaluating_threshold(self,*args):
        schedule_obj = schedule(
            'core.models.basemodels.thresholds.evaluate_threshold.evaluate',
            self.id,
            schedule_type=Schedule.MINUTES,
            repeats=-1,
            minutes= (self.recheck_interval/60)
        )
        # 1. Read metrics from monitoring engine
        # 2. Perform Actions if value reached the threshold limit
        self.schedule_id = schedule_obj.id
        self.save()

    def stop_evaluating_threshold(self,*args):
        try:
            Schedule.objects.filter(id=self.schedule_id).delete()
        except Schedule.DoesNotExist:
            pass
        finally:
            self.schedule_id = 0
            self.save()


