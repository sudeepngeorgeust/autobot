
from core.models.basemodels.base_model import BaseModel
from core.models.basemodels.application import Application
from django.db import models

class Instance(BaseModel):
    name = models.CharField(max_length=15, null=True)
    status = models.CharField(max_length=15, null=True)

    #To support multiple instances for a single application (One-to-Many from application to instance)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, related_name='instances')
