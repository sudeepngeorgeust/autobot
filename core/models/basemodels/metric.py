from core.models.basemodels.base_model import BaseModel
from django.db import models
from .application import Application


class Metric(BaseModel):
    name = models.CharField(max_length=20)
    unit = models.CharField(max_length=20,default='Count')
    aggragation = models.CharField(max_length=20,default='Average')
    
    #To support multiple instances for a single application (One-to-Many from application to instance)
    application = models.ForeignKey(Application, on_delete=models.CASCADE, related_name='metrics')