from .base_model import BaseModel
from django.db import models
from fernet_fields import EncryptedCharField

class Credential(BaseModel):
    name = models.CharField(max_length=25)

class SSHCredential(Credential):
    username = EncryptedCharField(max_length=15)
    password = EncryptedCharField(max_length=20)
    sshKey = EncryptedCharField(max_length=200)

    def __init__(self, username, password, sshKey, name):
        super(SSHCredential, self).__init__()
        self.username = username
        self.password = password
        self.sshKey = sshKey
        self.name = name
        self.date_created = timezone.now()
        self.date_modified = timezone.now()