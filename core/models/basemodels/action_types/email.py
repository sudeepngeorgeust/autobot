from core.models.basemodels import Action
from django.db import models

class Email(Action):
    email_id = models.EmailField(blank=False)
    subject = models.CharField(max_length=50, blank=True, null=True)
    body = models.CharField(max_length=250, blank=True, null=True)

    def perform_action(self,application):
        print("Email Action Perform")