from core.models.basemodels.actions import Action
from django.db import models
from django.core.validators import  MinValueValidator

class Scaler(Action):
    cool_off_period=models.IntegerField()
    minInstances=models.IntegerField(default=1)
    maxInstances=models.IntegerField(default=5)
    numInstances=models.IntegerField(default=1)

    available_actions = (('I','Increase Instance Count'),('D','Decrease Instance Count'))
    action_type = models.CharField(max_length=1,choices=available_actions,default='I')
    instance_step_count = models.IntegerField(default=1,validators=[MinValueValidator(0)])
       
    def perform_action(self,application):
        pass