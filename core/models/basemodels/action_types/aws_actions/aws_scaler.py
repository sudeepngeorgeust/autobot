from core.models.basemodels.action_types.scaler import Scaler
from core.models.aws.aws_sdk import *
from django.db import models

class AWSScaler(Scaler):
    AWS_INSTANCE_CHOICES = (
        ("t2.nano", "T2 NANO"),
        ("t2.micro", "T2 MICRO"),
        ("t2.small", "T2 SMALL"),
        ("t2.medium", "T2 MEDIUM"),
        ("t2.large", "T2 LARGE"),
        ("t2.xlarge", "T2 XLARGE"),
        ("t2.2xlarge", "T2 2XLARGE"),
        ("m4.large", "M4 LARGE"),
        ("m4.xlarge", "M4 XLARGE"),
        ("m4.2xlarge", "M4 2XLARGE"),
        ("m4.4xlarge", "M4 4XLARGE"),
        ("m4.10xlarge", "M4 10XLARGE"),
        ("m4.16xlarge", "M4 16XLARGE"),
        ("m3.medium", "M3 MEDIUM"),
        ("m3.large", "M3 LARGE"),
        ("m3.xlarge", "M3 XLARGE"),
        ("m3.2xlarge", "M3 2XLARGE")
    )
    amiId = models.CharField(max_length=15)
    instance_type = models.CharField(max_length=20, choices=AWS_INSTANCE_CHOICES, default='t2.nano')
    dryRun = models.BooleanField(default=0)
    SubnetId = models.CharField(max_length=20, null=True)
    SecurityGroupId = models.CharField(max_length=20, null=True)
    key_pair = models.CharField(max_length=25, null=True)

    class Meta:
        verbose_name = "AWS Scaler"

    def set_instance_count(self, number,application,aws_credential,instance_count):
        try:
            if (number >= 0):
                if (number > instance_count and number <= self.maxInstances):
                    created_instances = create_instances(self, number - instance_count, aws_credential)
                    add_instances_to_elb_classic(created_instances, application.loadbalancer, aws_credential)
                elif (number == instance_count):
                    pass
                elif (number < instance_count and number >= self.minInstances):
                    diff = instance_count - number
                    current = application.instances.instance_of(EC2)
                    terminate_instances(current[:diff], AWSCredential)
                    remove_instances_from_elb_classic(current[:diff], application.loadbalancer, AWSCredential)
            self.numInstances = len(application.loadbalancer.update_instances())
            self.save()
            self.save_base()

        except Exception as ex:
            logger.exception(ex)

        else:
            logger.info("setting instance count to :"+str(self.numInstances))
            return (self.numInstances)
        finally:
            pass

    def perform_action(self,application):
        instance_count = len(application.loadbalancer.update_instances(application))
        aws_credential = application.credential
        final_instance_count =  instance_count + self.instance_step_count
        if final_instance_count < 0 :
            final_instance_count = 0
        self.set_instance_count(final_instance_count,application,aws_credential,instance_count)
        super().perform_action(application)


