from core.models.basemodels.actions import Action
from django.db import models

class BlockIP(Action):
    ipAddress = models.CharField(max_length=50, blank=True, null=True)

    