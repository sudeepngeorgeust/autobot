from core.models.basemodels.exceptions import CoreError
class NoELBFound(CoreError):
    """

    """
    err = 'No Loadbalancers configured in this region'

class UnableToSetInstanceCount(CoreError):
    """

    """
    err = 'Unable To Set Instance Count'