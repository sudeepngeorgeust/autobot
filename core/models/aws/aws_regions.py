from djongo import models
from django.utils import timezone
from core.models.basemodels.base_model import BaseModel
class AWSRegions(BaseModel):
    name = models.CharField(max_length=100)
    code = models.CharField(max_length=100)

    def __str__(self):
        return self.name
