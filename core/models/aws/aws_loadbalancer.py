from core.models.basemodels.load_balancer import LoadBalancer
from .ec2_instance import EC2

class ELB_Classic(LoadBalancer):

    class Meta:
        verbose_name = "Elastic LoadBalancer Classic" 
    

    def update_instances(self,application):
        from .aws_sdk import get_compute_instances
        from .aws_credential import AWSCredential
        aws_credential = application.credential.get_real_instance()
        instance_ids = []
        instances = []
        for instance in get_compute_instances(self,aws_credential):
            ec2 = EC2.objects.get_or_create(instanceId=instance.instanceId)[0]
            ec2.status = instance.status
            ec2.save()
            ec2.save_base()
            instances.append(ec2)
            instance_ids.append(ec2.instanceId)
        EC2.objects.filter(application=application).exclude(instanceId__in=instance_ids).delete()
        application.instances.set(instances)
        application.save()
        application.save_base()
        return (application.instances.all())

    def list_instances(self):
        from .aws_sdk import get_compute_instances
        from .aws_credential import AWSCredential
        aws_credential = self.application.credentials.instance_of(AWSCredential)[0]
        return get_compute_instances(aws_credential, self)

