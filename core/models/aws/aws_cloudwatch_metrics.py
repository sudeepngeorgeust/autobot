from core.models.basemodels.metric import Metric
from djongo import models

class EC2CloudWatchMetric(Metric):
    value = models.CharField(max_length=40)
    namespace = models.CharField(max_length=20,default='AWS/EC2')
    dimensions = models.CharField(max_length=20,default='InstanceId')
    class Meta:
        verbose_name = "EC2 Cloud Watch Metric"
    def __str__(self):
        return self.namespace+ " | " + self.dimensions



class ELBCloudWatchMetric(Metric):
    value = models.CharField(max_length=40)
    namespace = models.CharField(max_length=20,default='AWS/ELB')
    dimensions = models.CharField(max_length=20,default='LoadBalancerName')
    class Meta:
        verbose_name = "ELB Cloud Watch Metric"

class ASGCloudWatchMetric(Metric):
    value = models.CharField(max_length=40)
    namespace = models.CharField(max_length=20,default='AWS/EC2')
    dimensions = models.CharField(max_length=20,default='AutoScalingGroupName')

    class Meta:
        verbose_name = "ASG Cloud Watch Metric"
