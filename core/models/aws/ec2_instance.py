from core.models.basemodels.instance import Instance
from djongo import models

class EC2(Instance):
    instanceId = models.CharField(max_length=20)

    class Meta:
        verbose_name = "EC2 Instance"

    def __str__(self):
        return self.instanceId
