from django.contrib import admin
from django.contrib.auth.models import Group
from core.models import *
from polymorphic.admin import PolymorphicInlineSupportMixin, StackedPolymorphicInline
from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter
from django.urls import include, path
from django.utils.html import format_html
from django.http import HttpResponseRedirect

admin.site.unregister(Group)
class ModelThresholdChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Threshold 

admin.site.register(SimpleThreshold)
class SimpleThresholdAdmin(ModelThresholdChildAdmin):
    base_model = SimpleThreshold
    exclude = ['']
    # define custom features here
class ThresholdParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Threshold 
    child_models = (SimpleThreshold,)
    list_filter = (PolymorphicChildModelFilter,)

# 1. Application

class ThresholdInline(StackedPolymorphicInline):
    """
    An inline for a polymorphic model.
    The actual form appearance of each row is determined by
    the child inline that corresponds with the actual model type.
    """
    class SimpleThresholdInline(StackedPolymorphicInline.Child):
        model = SimpleThreshold

    model = Threshold
    child_inlines = (
        SimpleThresholdInline,
    )
class ApplicationAdmin(admin.ModelAdmin):
    model=Application
    list_display = (
        'name',
        'loadbalancer',
        'credential',
        'metric_store',
        'application_actions', 
    )
    #inlines = (ThresholdInline,)
    exclude = ('monitoring_status',)
    def get_urls(self):
        urls = super(ApplicationAdmin, self).get_urls()
        my_urls = [
            path('toggle_monitoring/<int:id>', self.toggle_monitoring),
        ]
        return my_urls + urls

    def toggle_monitoring(self, request, id):
        obj = Application.objects.get(id = id)
        if obj != None:
            if obj.monitoring_status == "Not Running":
                obj.start_monitoring()
            else:
                obj.stop_monitoring()
        return HttpResponseRedirect("../")

    def application_actions(self, obj):
        button_text = format_html('<abbr title="Start">	&#x25ba;</abbr>' if obj.monitoring_status == "Not Running" else '<abbr title="Stop">&#9209;</abbr>')
        return format_html('<a href="toggle_monitoring/{}">{}</a>',obj.id,button_text)
    application_actions.short_description = 'Start/Stop Monitoring'
    application_actions.allow_tags = True
admin.site.register(Application,ApplicationAdmin)

# 2.Load balancer
class ModelLoadBalancerChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = LoadBalancer 

admin.site.register(ELB_Classic)
class ELBClassicAdmin(ModelLoadBalancerChildAdmin):
    base_model = ELB_Classic  # Explicitly set here!
    # define custom features here
class LoadBalancerParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = LoadBalancer 
    child_models = (ELBClassicAdmin,)
    list_filter = (PolymorphicChildModelFilter,) 

# 3.Credentials
class ModelCredentilasChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Credential 

admin.site.register(AWSCredential)

class AWSCredentialAdmin(ModelCredentilasChildAdmin):
    base_model = AWSCredential  # Explicitly set here!
    # define custom features here

class CredentialParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Credential 
    child_models = (AWSCredentialAdmin,)
    list_filter = (PolymorphicChildModelFilter,) 


# 4.Metric Store
class ModelMetricStoreChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = MetricStore 

admin.site.register(ElasticSearch)
class ElasticSearchAdmin(ModelMetricStoreChildAdmin):
    base_model = ElasticSearch  # Explicitly set here!
    # define custom features here
class MetricStoreParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = MetricStore 
    child_models = (ElasticSearchAdmin,)
    list_filter = (PolymorphicChildModelFilter,) 

# 5.Instances
class ModelInstancesChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Instance 

admin.site.register(EC2)
class EC2Admin(ModelInstancesChildAdmin):
    base_model = EC2  # Explicitly set here!
    # define custom features here
class InstanceParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Instance 
    child_models = (EC2Admin,)
    list_filter = (PolymorphicChildModelFilter,) 

# 5.Monitoring
class ModelMonitoringChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Monitoring 

admin.site.register(AWSCloudWatch)
class AWSCloudWatchAdmin(ModelMonitoringChildAdmin):
    base_model = AWSCloudWatch  # Explicitly set here!
    # define custom features here
class MonitoringParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Monitoring 
    child_models = (AWSCloudWatchAdmin,)
    list_filter = (PolymorphicChildModelFilter,) 

# 5.Metric
class ModelMetricChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Metric 

admin.site.register(EC2CloudWatchMetric)
class EC2CloudWatchMetricAdmin(ModelMetricChildAdmin):
    base_model = EC2CloudWatchMetric  # Explicitly set here!

admin.site.register(ASGCloudWatchMetric)
class ASGCloudWatchMetricAdmin(ModelMetricChildAdmin):
    base_model = ASGCloudWatchMetric  # Explicitly set here!

admin.site.register(ELBCloudWatchMetric)
class ELBCloudWatchMetricAdmin(ModelMetricChildAdmin):
    base_model = ELBCloudWatchMetric  # Explicitly set here!

class MonitoringParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Metric 
    child_models = (EC2CloudWatchMetricAdmin,ASGCloudWatchMetricAdmin,ELBCloudWatchMetricAdmin,)
    list_filter = (PolymorphicChildModelFilter,) 


# 5.Action
class ModelActionChildAdmin(PolymorphicChildModelAdmin):
    """ Base admin class for all child models """
    base_model = Action 

admin.site.register(AWSScaler)
class AWSScalerAdmin(ModelActionChildAdmin):
    base_model = AWSScaler  # Explicitly set here!

admin.site.register(Email)
class EmailAdmin(ModelActionChildAdmin):
    base_model = Email  # Explicitly set here!

admin.site.register(BlockIP)
class BlockIPAdmin(ModelActionChildAdmin):
    base_model = BlockIP  # Explicitly set here!

class MonitoringParentAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = Metric 
    child_models = (AWSScalerAdmin,EmailAdmin,BlockIPAdmin,)
    list_filter = (PolymorphicChildModelFilter,) 