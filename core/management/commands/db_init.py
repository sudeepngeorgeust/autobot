
    # thresholds = models.ManyToManyField(Threshold)
    # metric_store = models.ForeignKey(MetricStore,on_delete=models.PROTECT,blank=True,null=True)

from django.core.management.base import BaseCommand, CommandError
import datetime
from decimal import Decimal
from core.models import *

class Command(BaseCommand):
    args = ''
    help = 'Export data to remote server'

    def handle(self, *args, **options):
        # Create AWS App
        cred = AWSCredential.objects.get_or_create(name='Autobot AWS Account')[0]

        aws_region = AWSRegions.objects.get_or_create(code="us-east-1")[0]
        cred.region = aws_region
        cred.save()


        es = ElasticSearch.objects.get_or_create(host='35.153.231.229', port=9200, index='dev_metrics')[0]

        elb = ELB_Classic.objects.get_or_create(lb='ab8581fee9bae11e99a130a9a28e9ef8')[0]

        app = Application.objects.get_or_create(name='Autobot Test 1'
                                       ,loadbalancer=elb)[0]
        app.metric_store = es
        

        app.credential = cred
        # dashboard = KibanaDash.objects.get_or_create(dashboard_id="c9d863c0-ebe5-11e8-a727-d769b362df88",
        #                                              dashboard_url="/app/kibana#/dashboard/%s?embed=true&_g=(refreshInterval:(display:Off,pause:!f,value:0),time:(from:\'%s\',mode:absolute,to:\'%s\'))",
        #                                              host="35.153.231.229",
        #                                              port="5601",
        #                                              use_ssl=False,
        #                                              history = 180,
        #                                              future = 30,
        #                                              show_future = False)[0]
        # app.dashboard = dashboard
        instance = EC2.objects.get_or_create(instanceId="fgscdgsds" ,name="Instance1",status="inservice")[0]
        instance1 = EC2.objects.get_or_create(instanceId="1234fd" ,name="Instance1",status="outofservice")[0]
        instance2 = EC2.objects.get_or_create(instanceId="34535646" ,name="Instance2",status="outofservice")[0]
        instance3 = EC2.objects.get_or_create(instanceId="sfg" ,name="Instance3",status="outofservice")[0]

        app.instances.add(instance)
        app.instances.add(instance1)
        app.instances.add(instance2)
        app.instances.add(instance3)

        scaler = AWSScaler.objects.get_or_create(
            cool_off_period=60,
            amiId='ami-e7e54b98',
            instance_type="t2.nano",
            dryRun=False,
            SubnetId='subnet-b16fba9e',
            SecurityGroupId='sg-58af742d',
            key_pair='autobot_Test_app1',
        )[0]
        app.scaler = scaler

        acw = AWSCloudWatch.objects.get_or_create(period=600,keep_data=True)[0]
        app.monitoring_engines.set([acw])

        a = EC2CloudWatchMetric.objects.get_or_create(name='CPU Utilization',unit='percent', value='CPUUtilization', )[0]
        b = ELBCloudWatchMetric.objects.get_or_create(name='Request Count',unit='count' ,value='RequestCount', aggragation='Sum' )[0]
        app.metrics.add(a)
        app.metrics.add(b)

        app.enable_actions = True
    
        th = SimpleThreshold.objects.get_or_create(metric=a,monitoring_engine=acw)[0]
        th.value = Decimal("20.00")
        email  = Email.objects.get_or_create(email_id="s@g.com",subject="Test",body="Test test")[0]
        email.save()
        th.actions.set([scaler])
        th.save()

        th1 = SimpleThreshold.objects.get_or_create(metric=a,monitoring_engine=acw)[0]
        th1.value = Decimal("90.00")
        th1.actions.set([email])
        th1.save()

        th2 = SimpleThreshold.objects.get_or_create(metric=a,monitoring_engine=acw)[0]
        th.value = Decimal("80.00")
        th2.actions.set([scaler])
        th2.save()
        app.thresholds.set((th,th1,th2))
        app.save()
        app.start_monitoring()

       


