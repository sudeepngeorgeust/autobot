from django.core.management.base import BaseCommand, CommandError
from core.models import *
import datetime
import arrow

class Command(BaseCommand):
    args = ''
    help = "Load default data to DB"

    def handle(self, *args, **options):  
        AWS_REGION_CHOICES = (
            ('us-east-2', 'US East (Ohio)'),
            ('us-east-1', 'US East (N. Virginia)'),
            ('us-west-1', 'US West (N. California)'),
            ('us-west-2', 'US West (Oregon)'),
            ('ap-south-1', 'Asia Pacific (Mumbai)'),
            ('ap-northeast-2', 'Asia Pacific (Seoul)'),
            ('ap-southeast-1', 'Asia Pacific (Singapore)'),
            ('ap-southeast-2', 'Asia Pacific (Sydney)'),
            ('ap-northeast-1', 'Asia Pacific (Tokyo)'),
            ('ca-central-1', 'Canada (Central)'),
            ('eu-central-1', 'EU (Frankfurt)'),
            ('eu-west-1', 'EU (Ireland)'),
            ('eu-west-2', 'EU (London)'),
            ('sa-east-1', 'South America (São Paulo)')
        )
        for region in AWS_REGION_CHOICES:
            aws_region = AWSRegions(name=region[1], code=region[0])
            aws_region.save()